FROM golang:1.11

WORKDIR /go/src/app
COPY . .

RUN ./scripts/golang/build . example-server main.Version
RUN ./scripts/golang/run

## Build & Run
# docker build -t my-golang-app .
# docker run -it --rm --name my-running-app my-golang-app

## Compile
# docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.8 go build -v